"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DateUtil = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _moment2 = _interopRequireDefault(require("moment"));

var INPUT_DATE_FORMAT = 'DD/MM/YYYY';

var DateUtil =
/*#__PURE__*/
function () {
  function DateUtil() {
    (0, _classCallCheck2.default)(this, DateUtil);
  }

  (0, _createClass2.default)(DateUtil, null, [{
    key: "toHuman",

    /** formatters: **/

    /**
     * Returns provided date in human-friendly format
     * @param {string|date} date - date to format
     * @param {string|null} [format] - if date is provided in custom unknown format
     * @returns {string}
     */
    value: function toHuman(date) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return this._moment(date, format).calendar(null, {
        sameDay: '[Today at] HH:mm',
        lastDay: '[Yesterday at] HH:mm',
        lastWeek: '[Last] dddd [at] HH:mm',
        nextWeek: '[Next] dddd [at] HH:mm',
        sameElse: 'DD/MM/YYYY'
      });
    }
    /**
     * Returns provided date in ISO format
     * @param {string|date} date - date to format
     * @param {string|null} [format] - if date is provided in custom unknown format
     * @returns {string}
     */

  }, {
    key: "toISO",
    value: function toISO(date) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return this._moment(date, format).format(this.ISOFormat);
    }
    /**
     * Returns provided date in UNIX timestamp (in seconds)
     * @param {string|date} date - date to format
     * @param {string|null} [format] - if date is provided in custom unknown format
     * @returns {string}
     */

  }, {
    key: "toTimestamp",
    value: function toTimestamp(date) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return this._moment(date, format).format('X');
    }
    /**
     * Returns provided date in miliseconds
     * @param {string|date} date - date to format
     * @param {string|null} [format] - if date is provided in custom unknown format
     * @returns {string}
     */

  }, {
    key: "toMs",
    value: function toMs(date) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return this._moment(date, format).format('x');
    }
    /**
     * Returns provided date in input format
     * @param {string|date} date - date to format
     * @param {string|null} [format] - if date is provided in custom unknown format
     * @returns {string}
     */

  }, {
    key: "toInput",
    value: function toInput(date) {
      var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
      return this._moment(date, format).format(INPUT_DATE_FORMAT);
    }
  }, {
    key: "_moment",
    value: function _moment(date, format) {
      if (format) {
        return (0, _moment2.default)(date, format);
      }

      return (0, _moment2.default)(date);
    }
  }, {
    key: "ISOFormat",
    get: function get() {
      return 'YYYY-MM-DDT00:00:00+00:00';
    }
    /**
     * Returns minimum acceptable date for the app
     * @returns {string}
     */

  }, {
    key: "minDate",
    get: function get() {
      return this.toISO('01/01/1900');
    }
  }]);
  return DateUtil;
}();

exports.DateUtil = DateUtil;