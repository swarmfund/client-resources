"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MathUtil = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _freeze = _interopRequireDefault(require("@babel/runtime-corejs2/core-js/object/freeze"));

var BigNumber = require('bignumber.js');

var ROUNDING_MODES = (0, _freeze.default)({
  ROUND_UP: 0,
  ROUND_DOWN: 1,
  ROUND_CEIL: 2,
  ROUND_FLOOR: 3,
  ROUND_HALF_UP: 4,
  ROUND_HALF_DOWN: 5,
  ROUND_HALF_EVEN: 6,
  ROUND_HALF_CEIL: 7,
  ROUND_HALF_FLOOR: 8
});
var ONE = 1000000;
var DECIMAL_PLACES = 6;

var MathUtil =
/*#__PURE__*/
function () {
  function MathUtil() {
    (0, _classCallCheck2.default)(this, MathUtil);
  }

  (0, _createClass2.default)(MathUtil, null, [{
    key: "multiply",
    value: function multiply(a, b) {
      var ROUND_TYPE = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ROUNDING_MODES.ROUND_UP;
      if (!this._isValidParams('big-multiply', a, b)) return 0;
      BigNumber.config({
        ROUNDING_MODE: ROUND_TYPE
      });
      BigNumber.config({
        DECIMAL_PLACES: 0
      });
      var mul1 = new BigNumber(new BigNumber(a).times(new BigNumber(ONE)));
      var mul2 = new BigNumber(new BigNumber(b).times(new BigNumber(ONE)));
      var result = mul1.times(mul2);
      BigNumber.config({
        DECIMAL_PLACES: DECIMAL_PLACES
      });
      return result.dividedBy(new BigNumber(ONE)).dividedBy(new BigNumber(ONE)).toFixed(DECIMAL_PLACES);
    }
  }, {
    key: "divide",
    value: function divide(a, b) {
      var ROUND_TYPE = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ROUNDING_MODES.ROUND_UP;
      if (!this._isValidParams('big-divide', a, b)) return 0;
      BigNumber.config({
        ROUNDING_MODE: ROUND_TYPE
      });
      BigNumber.config({
        DECIMAL_PLACES: DECIMAL_PLACES
      });
      var num = new BigNumber(new BigNumber(a).times(new BigNumber(ONE)));
      var denum = new BigNumber(new BigNumber(b).times(new BigNumber(ONE)));
      var result = num.dividedBy(denum);
      return result.toFixed(DECIMAL_PLACES);
    }
  }, {
    key: "subtract",
    value: function subtract(a, b) {
      var ROUND_TYPE = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ROUNDING_MODES.ROUND_UP;
      if (!this._isValidParams('subtract', a, b)) return 0;
      BigNumber.config({
        ROUNDING_MODE: ROUND_TYPE
      });
      BigNumber.config({
        DECIMAL_PLACES: DECIMAL_PLACES
      });
      var one = new BigNumber(a);
      var two = new BigNumber(b);
      var result = one.sub(two);
      return result.toFixed(DECIMAL_PLACES);
    }
  }, {
    key: "add",
    value: function add(a, b) {
      var ROUND_TYPE = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : ROUNDING_MODES.ROUND_UP;
      if (!this._isValidParams('add', a, b)) return 0;
      BigNumber.config({
        ROUNDING_MODE: ROUND_TYPE
      });
      BigNumber.config({
        DECIMAL_PLACES: DECIMAL_PLACES
      });
      var one = new BigNumber(a);
      var two = new BigNumber(b);
      var result = one.add(two);
      return result.toFixed(DECIMAL_PLACES);
    }
  }, {
    key: "_isValidParams",
    value: function _isValidParams(op, a, b) {
      var c = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 0;

      try {
        new BigNumber(a);
        new BigNumber(b);
        new BigNumber(c);
      } catch (err) {
        return false;
      }

      return true;
    }
  }, {
    key: "round",
    value: function round(n) {
      return Math.round(n);
    }
  }]);
  return MathUtil;
}();

exports.MathUtil = MathUtil;