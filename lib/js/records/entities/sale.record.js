"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SaleRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _moment = _interopRequireDefault(require("moment"));

var _get2 = _interopRequireDefault(require("lodash/get"));

var _date = require("../../utils/date.util");

var STATES = {
  Open: 1,
  Closed: 2,
  Cancelled: 4,
  Promotion: 8,
  Voting: 16
};

var SaleRecord =
/*#__PURE__*/
function () {
  function SaleRecord(record, details) {
    (0, _classCallCheck2.default)(this, SaleRecord);
    this._record = record;
    this.id = record.id;
    this.owner = (0, _get2.default)(record, 'ownerId');
    this.baseAsset = (0, _get2.default)(record, 'baseAsset');
    this.defaultQuoteAsset = (0, _get2.default)(record, 'defaultQuoteAsset');
    this.quoteAssets = (0, _get2.default)(record, 'quoteAssets.quoteAssets') || [];
    this.baseHardCap = (0, _get2.default)(record, 'baseHardCap');
    this.startTime = (0, _get2.default)(record, 'startTime');
    this.endTime = (0, _get2.default)(record, 'endTime');
    this.softCap = (0, _get2.default)(record, 'softCap');
    this.hardCap = (0, _get2.default)(record, 'hardCap');
    this.currentCap = (0, _get2.default)(record, 'currentCap');
    this.statistics = (0, _get2.default)(record, 'statistics');
    this.investors = (0, _get2.default)(record, 'statistics.investors');
    this.averageInvestment = (0, _get2.default)(record, 'statistics.averageAmount');
    this.state = (0, _get2.default)(record, 'state');
    this.stateValue = (0, _get2.default)(record, 'state.value');
    this.stateStr = (0, _get2.default)(record, 'state.name');
    this.details = (0, _get2.default)(this._record, 'details');
    this.name = (0, _get2.default)(record, 'details.name');
    this.description = (0, _get2.default)(record, 'details.description');
    this.shortDescription = (0, _get2.default)(record, 'details.shortDescription');
    this.youtubeVideoId = (0, _get2.default)(record, 'details.youtubeVideoId');
    this.returnOfInvestment = (0, _get2.default)(record, 'details.returnOfInvestment');
    this.returnOfInvestmentFrom = (0, _get2.default)(record, 'details.returnOfInvestment.from');
    this.returnOfInvestmentTo = (0, _get2.default)(record, 'details.returnOfInvestment.to');
    this.logo = (0, _get2.default)(this._record, 'details.logo');
    this.logoKey = (0, _get2.default)(this._record, 'details.logo.key');
    this.logoName = (0, _get2.default)(this._record, 'details.logo.name');
    this.logoType = (0, _get2.default)(this._record, 'details.logo.type');
  }

  (0, _createClass2.default)(SaleRecord, [{
    key: "acceptsQuote",
    value: function acceptsQuote(assetCode) {
      return this.quoteAssetCodes.indexOf(assetCode) !== -1;
    }
  }, {
    key: "isInState",

    /** states: **/
    value: function isInState(state) {
      return this.state === state;
    }
  }, {
    key: "returnOfInvestmentStr",
    get: function get() {
      if (!this.returnOfInvestmentFrom && !this.returnOfInvestmentTo) return '';
      if (this.returnOfInvestmentFrom && !this.returnOfInvestmentTo) return "".concat(this.returnOfInvestmentFrom, "%+");
      if (!this.returnOfInvestmentFrom && this.returnOfInvestmentTo) return "under ".concat(this.returnOfInvestmentTo, "%");
      if (this.returnOfInvestmentFrom && this.returnOfInvestmentTo) return "".concat(this.returnOfInvestmentFrom, "\u2014").concat(this.returnOfInvestmentTo);
    }
    /** quote assets: **/

  }, {
    key: "quoteAssetCodes",
    get: function get() {
      return this.quoteAssets.map(function (asset) {
        return asset.asset;
      });
    }
  }, {
    key: "quoteAssetPrices",
    get: function get() {
      return this.quoteAssets.reduce(function (prices, asset) {
        prices[asset.asset] = asset.price;
        return prices;
      }, {});
    }
  }, {
    key: "currentCaps",
    get: function get() {
      return this.quoteAssets.reduce(function (caps, asset) {
        caps[asset.asset] = asset.currentCap;
        return caps;
      }, {});
    }
  }, {
    key: "totalCurrentCaps",
    get: function get() {
      return this.quoteAssets.reduce(function (caps, asset) {
        caps[asset.asset] = asset.totalCurrentCap;
        return caps;
      }, {});
    }
  }, {
    key: "hardCaps",
    get: function get() {
      return this.quoteAssets.reduce(function (caps, asset) {
        caps[asset.asset] = asset.hardCap;
        return caps;
      }, {});
    }
  }, {
    key: "acceptsBTC",
    get: function get() {
      return this.acceptsQuote('BTC');
    }
  }, {
    key: "acceptsETH",
    get: function get() {
      return this.acceptsQuote('ETH');
    }
  }, {
    key: "acceptsDAI",
    get: function get() {
      return this.acceptsQuote('DAI');
    }
  }, {
    key: "acceptsDASH",
    get: function get() {
      return this.acceptsQuote('DASH');
    }
  }, {
    key: "isPromotion",
    get: function get() {
      return this.isInState(STATES.Promotion);
    }
  }, {
    key: "isVoting",
    get: function get() {
      return this.isInState(STATES.Voting);
    }
  }, {
    key: "isOpened",
    get: function get() {
      return this.isInState(STATES.Open);
    }
  }, {
    key: "isClosed",
    get: function get() {
      return this.isInState(STATES.Closed);
    }
  }, {
    key: "isCanceled",
    get: function get() {
      return this.isInState(STATES.Cancelled);
    }
  }, {
    key: "isUpcoming",
    get: function get() {
      return (0, _moment.default)(this.startTime).isAfter((0, _moment.default)());
    }
    /** progress info: **/

  }, {
    key: "daysToGo",
    get: function get() {
      return (0, _moment.default)(this.endTime).diff((0, _moment.default)(), 'days');
    }
  }, {
    key: "startsIn",
    get: function get() {
      return (0, _moment.default)(this.startTime).diff((0, _moment.default)().startOf('day'), 'days');
    }
  }, {
    key: "hardCapProgress",
    get: function get() {
      return Math.round(this.currentCap / this.hardCap / 100) * 10000;
    }
  }, {
    key: "softCapProgress",
    get: function get() {
      return Math.round(this.currentCap / this.softCap / 100) * 10000;
    }
  }, {
    key: "left",
    get: function get() {
      if (this.isUpcoming) {
        if (this.startsIn === 0) return "Starts today at ".concat(_date.DateUtil.toHuman(this.startTime));
        return "Starts in ".concat(this.startsIn, " day").concat(this.startsIn === 1 ? '' : 's');
      }

      if (this.isClosed) return "Finished";
      if (this.isCanceled) return 'Canceled';
      if (this.daysToGo === 0) return "Ends ".concat(_date.DateUtil.toHuman(this.endTime));
      if (this.daysToGo > 0) return "".concat(this.daysToGo, " days left");
    }
  }, {
    key: "leftHtml",
    get: function get() {
      var span = function span(txt) {
        return "<span class=\"left-number\">".concat(txt, "</span>");
      };

      if (this.isUpcoming) {
        if (this.startsIn === 0) return "Starts today at ".concat(span(_date.DateUtil.toHuman(this.startTime)));
        return "Starts in ".concat(span("".concat(this.startsIn, " day").concat(this.startsIn === 1 ? '' : 's')));
      }

      if (this.isClosed) return "Finished";
      if (this.isCanceled) return 'Canceled';
      if (this.daysToGo === 0) return "Ends ".concat(span(_date.DateUtil.toHuman(this.endTime)));
      if (this.daysToGo > 0) return "".concat(span(this.daysToGo), " days left");
    }
  }]);
  return SaleRecord;
}();

exports.SaleRecord = SaleRecord;