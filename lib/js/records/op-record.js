"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.OpRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var PAYMENT_STATES = {
  pending: 'processing',
  rejected: 'failed'
};

var OpRecord = function OpRecord(record) {
  (0, _classCallCheck2.default)(this, OpRecord);
  this._record = record;
  this.id = record.id;
  this.date = record.ledgerCloseTime;
  this.state = PAYMENT_STATES[record.state] || record.state;
};

exports.OpRecord = OpRecord;