"use strict";

var _test = require("../test");

var _saleCreate = require("./sale-create.record");

describe('SaleRequestRecord', function () {
  var rawRecord, responseRecord, parsedRecord;

  var parse = function parse() {
    responseRecord = _test.MockFactory.horizon(rawRecord);
    parsedRecord = new _saleCreate.SaleRequestRecord(responseRecord);
  };

  var setQuoteAssets = function setQuoteAssets(assets) {
    rawRecord.details.sale.quote_assets = assets;
    parse();
  };

  var setReturnOfInvestment = function setReturnOfInvestment(_ref) {
    var from = _ref.from,
        to = _ref.to;
    rawRecord.details.sale.details.return_of_investment.from = from;
    rawRecord.details.sale.details.return_of_investment.to = to;
    parse();
  };

  var setSaleType = function setSaleType(type) {
    rawRecord.details.sale.sale_type = type;
    parse();
  };

  var setLogoKey = function setLogoKey(key) {
    rawRecord.details.sale.details.logo = {};
    rawRecord.details.sale.details.logo.key = key;
    parse();
  };

  var setStartTime = function setStartTime(time) {
    rawRecord.details.sale.start_time = time;
    parse();
  };

  var setEndTime = function setEndTime(time) {
    rawRecord.details.sale.end_time = time;
    parse();
  };

  beforeEach(function () {
    rawRecord = _test.MockFactory.saleCreate;
    parse();
  });
  it('constructor should properly set all the basic fields', function () {
    expect(parsedRecord.baseAsset).to.equal(rawRecord.details.sale.base_asset);
    expect(parsedRecord.name).to.equal(rawRecord.details.sale.details.name);
    expect(parsedRecord.defaultQuoteAsset).to.equal(rawRecord.details.sale.default_quote_asset);
    expect(parsedRecord.startTime).to.equal(rawRecord.details.sale.start_time);
    expect(parsedRecord.endTime).to.equal(rawRecord.details.sale.end_time);
    expect(parsedRecord.softCap).to.equal(rawRecord.details.sale.soft_cap);
    expect(parsedRecord.hardCap).to.equal(rawRecord.details.sale.hard_cap);
    expect(parsedRecord.baseAssetForHardCap).to.equal(rawRecord.details.sale.base_asset_for_hard_cap);
  });
  it('constructor should properly set sale state fields', function () {
    expect(parsedRecord.saleState).to.equal(rawRecord.details.sale.state.value);
    expect(parsedRecord.saleStateStr).to.equal(rawRecord.details.sale.state.name);
  });
  it('constructor should properly set sale type fields', function () {
    expect(parsedRecord.saleTypeStr).to.equal(rawRecord.details.sale.sale_type.name);
    expect(parsedRecord.saleType).to.equal(rawRecord.details.sale.sale_type.value);
  });
  it('constructor should properly set all the details fields', function () {
    expect(parsedRecord.description).to.equal(rawRecord.details.sale.details.description);
    expect(parsedRecord.shortDescription).to.equal(rawRecord.details.sale.details.short_description);
    expect(parsedRecord.youtubeVideoId).to.equal(rawRecord.details.sale.details.youtube_video_id);
    expect(parsedRecord.returnOfInvestmentFrom).to.equal(rawRecord.details.sale.details.return_of_investment.from);
    expect(parsedRecord.returnOfInvestmentTo).to.equal(rawRecord.details.sale.details.return_of_investment.to);
  });
  it('constructor should properly set all the quote assets', function () {
    var quoteAssets = [{
      quote_asset: "ETH",
      price: "1.000000"
    }, {
      quote_asset: "BTC",
      price: "1.000000"
    }, {
      quote_asset: "DASH",
      price: "1.000000"
    }];
    var expectedResult = ["ETH", "BTC", "DASH"];
    setQuoteAssets(quoteAssets);
    expect(parsedRecord.quoteAssets).to.deep.equal(expectedResult);
  });
  it('constructor should properly logo details', function () {
    expect(parsedRecord.logoKey).to.equal(rawRecord.details.sale.details.logo.key);
    expect(parsedRecord.logoName).to.equal(rawRecord.details.sale.details.logo.name);
    expect(parsedRecord.logoType).to.equal(rawRecord.details.sale.details.logo.type);
  });
  it('logoUrl getter returns proper value', function () {
    var storageUrl = 'https://storage.com';
    var key = 'fooEq112ewq134qweq41weqweqwe';
    setLogoKey(key);
    expect(parsedRecord.logoUrl(storageUrl)).to.equal("".concat(storageUrl, "/").concat(key));
  });
  describe('opts() method should return proper data', function () {
    var opts;
    beforeEach(function () {
      opts = parsedRecord.opts();
    });
    it('requestID', function () {
      expect(opts.requestID).to.equal(rawRecord.id);
    });
    it('baseAsset', function () {
      expect(opts.baseAsset).to.equal(rawRecord.details.sale.base_asset);
    });
    it('defaultQuoteAsset', function () {
      expect(opts.defaultQuoteAsset).to.equal(rawRecord.details.sale.default_quote_asset);
    });
    it('startTime', function () {
      var time = '2018-06-19T21:00:00Z';
      var expectedResult = '1529442000';
      setStartTime(time);
      opts = parsedRecord.opts();
      expect(opts.startTime).to.equal(expectedResult);
    });
    it('endTime', function () {
      var time = '2018-06-29T21:00:00Z';
      var expectedResult = '1530306000';
      setEndTime(time);
      opts = parsedRecord.opts();
      expect(opts.endTime).to.equal(expectedResult);
    });
    it('softCap', function () {
      expect(opts.softCap).to.equal(rawRecord.details.sale.soft_cap);
    });
    it('hardCap', function () {
      expect(opts.hardCap).to.equal(rawRecord.details.sale.hard_cap);
    });
    it('baseAssetForHardCap', function () {
      expect(opts.baseAssetForHardCap).to.equal(rawRecord.details.sale.base_asset_for_hard_cap);
    });
    it('saleStaten', function () {
      expect(opts.saleState).to.equal(rawRecord.details.sale.state.value);
    });
    it('details.name', function () {
      expect(opts.details.name).to.equal(rawRecord.details.sale.details.name);
    });
    it('details.short_description', function () {
      expect(opts.details.short_description).to.equal(rawRecord.details.sale.details.short_description);
    });
    it('details.description', function () {
      expect(opts.details.description).to.equal(rawRecord.details.sale.details.description);
    });
    it('quoteAssets', function () {
      var quoteAssets = [{
        quote_asset: "ETH",
        price: "1.000000"
      }, {
        quote_asset: "DAI",
        price: "1.000000"
      }];
      var expectedResult = [{
        asset: "ETH",
        price: "1"
      }, {
        asset: "DAI",
        price: "1"
      }];
      setQuoteAssets(quoteAssets);
      opts = parsedRecord.opts();
      expect(opts.quoteAssets).to.deep.equal(expectedResult);
    });
    it('returnOfInvestment', function () {
      var from = '30';
      var to = '91';
      setReturnOfInvestment({
        from: from,
        to: to
      });
      opts = parsedRecord.opts();
      expect(opts.details.return_of_investment.from).to.be.equal(from);
      expect(opts.details.return_of_investment.to).to.be.equal(to);
    });
    it('saleType', function () {
      var value = {
        name: "fixed_price",
        value: 3
      };
      var expectedResult = 3;
      setSaleType(value);
      opts = parsedRecord.opts();
      expect(opts.saleType).to.equal(expectedResult);
    });
    it('logo', function () {
      expect(opts.details.logo.key).to.equal(rawRecord.details.sale.details.logo.key);
      expect(opts.details.logo.type).to.equal(rawRecord.details.sale.details.logo.type);
      expect(opts.details.logo.name).to.equal(rawRecord.details.sale.details.logo.name);
    });
  });
});