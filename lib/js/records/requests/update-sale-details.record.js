"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.UpdateSaleDetailsRequestRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _requestRecord = require("../request-record");

var _get2 = _interopRequireDefault(require("lodash/get"));

var UpdateSaleDetailsRequestRecord =
/*#__PURE__*/
function (_RequestRecord) {
  (0, _inherits2.default)(UpdateSaleDetailsRequestRecord, _RequestRecord);

  function UpdateSaleDetailsRequestRecord(record, details) {
    var _this;

    (0, _classCallCheck2.default)(this, UpdateSaleDetailsRequestRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(UpdateSaleDetailsRequestRecord).call(this, record));
    _this.saleId = (0, _get2.default)(record, 'details.updateSaleDetails.saleId');
    _this.name = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.name');
    _this.shortDescription = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.shortDescription');
    _this.logo = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.logo');
    _this.logoKey = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.logo.key');
    _this.logoName = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.logo.name');
    _this.logoType = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.logo.type');
    _this.description = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.description');
    _this.youtubeVideoId = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.youtubeVideoId');
    _this.returnOfInvestment = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.returnOfInvestment');
    _this.returnOfInvestmentFrom = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.returnOfInvestment.from');
    _this.returnOfInvestmentTo = (0, _get2.default)(record, 'details.updateSaleDetails.newDetails.returnOfInvestment.to');
    return _this;
  }

  return UpdateSaleDetailsRequestRecord;
}(_requestRecord.RequestRecord);

exports.UpdateSaleDetailsRequestRecord = UpdateSaleDetailsRequestRecord;