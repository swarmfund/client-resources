"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.SaleRequestRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _requestRecord = require("../request-record");

var _date = require("../../utils/date.util");

var _get2 = _interopRequireDefault(require("lodash/get"));

var SaleRequestRecord =
/*#__PURE__*/
function (_RequestRecord) {
  (0, _inherits2.default)(SaleRequestRecord, _RequestRecord);

  function SaleRequestRecord(record) {
    var _this;

    var details = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck2.default)(this, SaleRequestRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(SaleRequestRecord).call(this, record));
    _this.baseAsset = (0, _get2.default)(_this._record, 'details.sale.baseAsset');
    _this.name = (0, _get2.default)(_this._record, 'details.sale.details.name');
    _this.defaultQuoteAsset = (0, _get2.default)(_this._record, 'details.sale.defaultQuoteAsset');
    _this.startTime = (0, _get2.default)(_this._record, 'details.sale.startTime');
    _this.endTime = (0, _get2.default)(_this._record, 'details.sale.endTime');
    _this.softCap = (0, _get2.default)(_this._record, 'details.sale.softCap');
    _this.hardCap = (0, _get2.default)(_this._record, 'details.sale.hardCap');
    _this.baseAssetForHardCap = (0, _get2.default)(_this._record, 'details.sale.baseAssetForHardCap');
    _this.saleTypeStr = (0, _get2.default)(_this._record, 'details.sale.saleType.name');
    _this.saleType = (0, _get2.default)(_this._record, 'details.sale.saleType.value');
    _this.saleState = (0, _get2.default)(_this._record, 'details.sale.state.value');
    _this.saleStateStr = (0, _get2.default)(_this._record, 'details.sale.state.name');
    _this.details = (0, _get2.default)(_this._record, 'details.sale.details');
    _this.description = (0, _get2.default)(_this._record, 'details.sale.details.description');
    _this.logo = (0, _get2.default)(_this._record, 'details.sale.details.logo');
    _this.logoKey = (0, _get2.default)(_this._record, 'details.sale.details.logo.key');
    _this.logoName = (0, _get2.default)(_this._record, 'details.sale.details.logo.name');
    _this.logoType = (0, _get2.default)(_this._record, 'details.sale.details.logo.type');
    _this.shortDescription = (0, _get2.default)(_this._record, 'details.sale.details.shortDescription');
    _this.youtubeVideoId = (0, _get2.default)(_this._record, 'details.sale.details.youtubeVideoId');
    _this.returnOfInvestment = (0, _get2.default)(record, 'details.sale.details.returnOfInvestment');
    _this.returnOfInvestmentFrom = (0, _get2.default)(record, 'details.sale.details.returnOfInvestment.from');
    _this.returnOfInvestmentTo = (0, _get2.default)(record, 'details.sale.details.returnOfInvestment.to');
    _this.quoteAssets = _this._getQuoteAssets();
    return _this;
  }

  (0, _createClass2.default)(SaleRequestRecord, [{
    key: "logoUrl",
    value: function logoUrl(storageUrl) {
      return this.logoKey ? "".concat(storageUrl, "/").concat(this.logoKey) : '';
    }
  }, {
    key: "_getQuoteAssets",
    value: function _getQuoteAssets() {
      return (0, _get2.default)(this._record, 'details.sale.quoteAssets', []).map(function (asset) {
        return asset.quoteAsset;
      });
    }
  }, {
    key: "opts",
    value: function opts() {
      return {
        requestID: this.id,
        baseAsset: this.baseAsset,
        defaultQuoteAsset: this.defaultQuoteAsset,
        startTime: _date.DateUtil.toTimestamp(this.startTime),
        endTime: _date.DateUtil.toTimestamp(this.endTime),
        softCap: this.softCap,
        hardCap: this.hardCap,
        saleState: this.saleState,
        baseAssetForHardCap: this.baseAssetForHardCap,
        details: {
          name: this.name,
          short_description: this.shortDescription,
          description: this.description,
          logo: this.logo,
          return_of_investment: {
            to: this.returnOfInvestmentTo,
            from: this.returnOfInvestmentFrom
          }
        },
        quoteAssets: this.quoteAssets.map(function (asset) {
          return {
            asset: asset,
            price: '1'
          };
        }),
        saleType: this.saleType
      };
    }
  }, {
    key: "returnOfInvestmentStr",
    get: function get() {
      if (!this.returnOfInvestmentFrom && !this.returnOfInvestmentTo) return '';
      if (this.returnOfInvestmentFrom && !this.returnOfInvestmentTo) return "".concat(this.returnOfInvestmentFrom, "%+");
      if (!this.returnOfInvestmentFrom && this.returnOfInvestmentTo) return "under ".concat(this.returnOfInvestmentTo, "%");
      if (this.returnOfInvestmentFrom && this.returnOfInvestmentTo) return "".concat(this.returnOfInvestmentFrom, "\u2014").concat(this.returnOfInvestmentTo);
    }
  }]);
  return SaleRequestRecord;
}(_requestRecord.RequestRecord);

exports.SaleRequestRecord = SaleRequestRecord;