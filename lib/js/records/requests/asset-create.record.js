"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.AssetCreateRequestRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _swarmNewSdk = require("swarm-new-sdk");

var _requestRecord = require("../request-record");

var _get2 = _interopRequireDefault(require("lodash/get"));

var AssetCreateRequestRecord =
/*#__PURE__*/
function (_RequestRecord) {
  (0, _inherits2.default)(AssetCreateRequestRecord, _RequestRecord);

  function AssetCreateRequestRecord(record) {
    var _this;

    var details = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck2.default)(this, AssetCreateRequestRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(AssetCreateRequestRecord).call(this, record));
    _this.assetName = (0, _get2.default)(_this._record, 'details.assetCreate.details.name');
    _this.assetCode = (0, _get2.default)(_this._record, 'details.assetCreate.code');
    _this.preissuedAssetSigner = (0, _get2.default)(_this._record, 'details.assetCreate.preIssuedAssetSigner');
    _this.maxIssuanceAmount = (0, _get2.default)(_this._record, 'details.assetCreate.maxIssuanceAmount');
    _this.initialPreissuedAmount = (0, _get2.default)(_this._record, 'details.assetCreate.initialPreissuedAmount');
    _this.policies = _this._policies();
    _this.policy = _this._policy();
    _this.details = (0, _get2.default)(_this._record, 'details.assetCreate.details');
    _this.terms = (0, _get2.default)(_this._record, 'details.assetCreate.details.terms');
    _this.termsKey = (0, _get2.default)(_this._record, 'details.assetCreate.details.terms.key');
    _this.termsName = (0, _get2.default)(_this._record, 'details.assetCreate.details.terms.name');
    _this.termsType = (0, _get2.default)(_this._record, 'details.assetCreate.details.terms.type');
    _this.logo = (0, _get2.default)(_this._record, 'details.assetCreate.details.logo');
    _this.logoKey = (0, _get2.default)(_this._record, 'details.assetCreate.details.logo.key');
    _this.logoName = (0, _get2.default)(_this._record, 'details.assetCreate.details.logo.name');
    _this.logoType = (0, _get2.default)(_this._record, 'details.assetCreate.details.logo.type');
    _this.externalSystemType = (0, _get2.default)(_this._record, 'details.assetCreate.details.externalSystemType');
    _this.attachedDetails = details;
    return _this;
  }

  (0, _createClass2.default)(AssetCreateRequestRecord, [{
    key: "logoUrl",
    value: function logoUrl(storageUrl) {
      return this.logoKey ? "".concat(storageUrl, "/").concat(this.logoKey) : '';
    }
  }, {
    key: "termsUrl",
    value: function termsUrl(storageUrl) {
      return this.termsKey ? "".concat(storageUrl, "/").concat(this.termsKey) : '';
    }
  }, {
    key: "_policies",
    value: function _policies() {
      return (0, _get2.default)(this._record, 'details.assetCreate.policies', []).map(function (policy) {
        return policy.value;
      });
    }
  }, {
    key: "_policy",
    value: function _policy() {
      return (0, _get2.default)(this._record, 'details.assetCreate.policies', []).map(function (p) {
        return p.value;
      }).reduce(function (s, p) {
        return s | p;
      }, 0);
    }
  }, {
    key: "opts",

    /**
     * Composes opts for operation {@link ManageAssetBuilder.assetCreationRequest}
     *
     * @returns {object}
     */
    value: function opts() {
      return {
        requestID: this.id,
        code: this.assetCode,
        preissuedAssetSigner: this.preissuedAssetSigner,
        maxIssuanceAmount: this.maxIssuanceAmount,
        policies: this.policy,
        initialPreissuedAmount: this.initialPreissuedAmount,
        details: {
          name: this.assetName,
          logo: {
            key: this.logoKey,
            name: this.logoName,
            type: this.logoType
          },
          terms: {
            key: this.termsKey,
            name: this.termsName,
            type: this.termsType
          }
        }
      };
    }
  }, {
    key: "isBaseAsset",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.baseAsset);
    }
  }, {
    key: "isDepositable",
    get: function get() {
      return !!this.externalSystemType;
    }
  }, {
    key: "isIssuanceManualReviewRequired",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.issuanceManualReviewRequired);
    }
  }, {
    key: "isRequiresKYC",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.requiresKyc);
    }
  }, {
    key: "isStatsQuoteAsset",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.statsQuoteAsset);
    }
  }, {
    key: "isTwoStepWithdrawal",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.twoStepWithdrawal);
    }
  }, {
    key: "isTransferable",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.transferable);
    }
  }, {
    key: "isWithdrawable",
    get: function get() {
      return !!(this.policy & _swarmNewSdk.ASSET_POLICIES.withdrawable);
    }
  }]);
  return AssetCreateRequestRecord;
}(_requestRecord.RequestRecord);

exports.AssetCreateRequestRecord = AssetCreateRequestRecord;