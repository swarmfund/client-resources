"use strict";

var _assetCreate = require("./asset-create.record");

var _swarmNewSdk = require("swarm-new-sdk");

var _test = require("../test");

describe('AssetCreateRequestRecord', function () {
  var rawRecord, responseRecord, parsedRecord;

  var parse = function parse() {
    responseRecord = _test.MockFactory.horizon(rawRecord);
    parsedRecord = new _assetCreate.AssetCreateRequestRecord(responseRecord);
  };

  var setPolicies = function setPolicies() {
    for (var _len = arguments.length, policies = new Array(_len), _key = 0; _key < _len; _key++) {
      policies[_key] = arguments[_key];
    }

    rawRecord.details.asset_create.policies = policies.map(function (p) {
      return {
        value: p,
        name: 'SomePolicyNameWeDoNotInterestedIn'
      };
    });
    parse();
  };

  beforeEach(function () {
    rawRecord = _test.MockFactory.assetCreate;
    parse();
  });
  it('constructor should properly set all the basic fields', function () {
    expect(parsedRecord.assetName).to.equal(rawRecord.details.asset_create.details.name);
    expect(parsedRecord.assetCode).to.equal(rawRecord.details.asset_create.code);
    expect(parsedRecord.preissuedAssetSigner).to.equal(rawRecord.details.asset_create.pre_issued_asset_signer);
    expect(parsedRecord.maxIssuanceAmount).to.equal(rawRecord.details.asset_create.max_issuance_amount);
    expect(parsedRecord.initialPreissuedAmount).to.equal(rawRecord.details.asset_create.initial_preissued_amount);
    expect(parsedRecord.externalSystemType).to.deep.equal(rawRecord.details.asset_create.details.external_system_type);
  });
  it('constructor should properly set policies fields', function () {
    setPolicies(2, 4, 8);
    var expectedResult = [2, 4, 8];
    var expectedPolicyValue = 14;
    expect(parsedRecord.policies).to.deep.equal(expectedResult);
    expect(parsedRecord.policy).to.equal(expectedPolicyValue);
  });
  it('constructor should properly logo details', function () {
    expect(parsedRecord.logo).to.deep.equal(rawRecord.details.asset_create.details.logo);
    expect(parsedRecord.logoKey).to.equal(rawRecord.details.asset_create.details.logo.key);
    expect(parsedRecord.logoName).to.equal(rawRecord.details.asset_create.details.logo.name);
    expect(parsedRecord.logoType).to.equal(rawRecord.details.asset_create.details.logo.type);
  });
  it('constructor should properly terms details', function () {
    expect(parsedRecord.terms).to.deep.equal(rawRecord.details.asset_create.details.terms);
    expect(parsedRecord.termsKey).to.equal(rawRecord.details.asset_create.details.terms.key);
    expect(parsedRecord.termsName).to.equal(rawRecord.details.asset_create.details.terms.name);
    expect(parsedRecord.termsType).to.equal(rawRecord.details.asset_create.details.terms.type);
  });
  describe('getters should return proper values', function () {
    var setExternalSystemType = function setExternalSystemType(type) {
      rawRecord.details.asset_create.external_system_type = type;
      parse();
    };

    it('isBaseAsset', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.baseAsset);
      expect(parsedRecord.isBaseAsset).to.equal(true);
      setPolicies([]);
      expect(parsedRecord.isBaseAsset).to.equal(false);
    });
    it('isIssuanceManualReviewRequired', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.issuanceManualReviewRequired);
      expect(parsedRecord.isIssuanceManualReviewRequired).to.equal(true);
      setPolicies();
      expect(parsedRecord.isIssuanceManualReviewRequired).to.equal(false);
    });
    it('isRequiresKYC', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.requiresKyc);
      expect(parsedRecord.isRequiresKYC).to.equal(true);
      setPolicies();
      expect(parsedRecord.isRequiresKYC).to.equal(false);
    });
    it('isStatsQuoteAsset', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.statsQuoteAsset);
      expect(parsedRecord.isStatsQuoteAsset).to.equal(true);
      setPolicies();
      expect(parsedRecord.isStatsQuoteAsset).to.equal(false);
    });
    it('isTwoStepWithdrawal', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.twoStepWithdrawal);
      expect(parsedRecord.isTwoStepWithdrawal).to.equal(true);
      setPolicies();
      expect(parsedRecord.isTwoStepWithdrawal).to.equal(false);
    });
    it('isTransferable', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.transferable);
      expect(parsedRecord.isTransferable).to.equal(true);
      setPolicies();
      expect(parsedRecord.isTransferable).to.equal(false);
    });
    it('isWithdrawable', function () {
      setPolicies(_swarmNewSdk.ASSET_POLICIES.withdrawable);
      expect(parsedRecord.isWithdrawable).to.equal(true);
      setPolicies();
      expect(parsedRecord.isWithdrawable).to.equal(false);
    });
    it('isDepositable', function () {
      setExternalSystemType('4');
      expect(parsedRecord.isDepositable).to.equal(true);
      setExternalSystemType(undefined);
      expect(parsedRecord.isDepositable).to.equal(true);
    });
  });
  describe('URL getters should return proper values', function () {
    var setLogoKey = function setLogoKey(key) {
      rawRecord.details.asset_create.details.logo = {};
      rawRecord.details.asset_create.details.logo.key = key;
      parse();
    };

    var setTermsKey = function setTermsKey(key) {
      rawRecord.details.asset_create.details.terms = {};
      rawRecord.details.asset_create.details.terms.key = key;
      parse();
    };

    var storageUrl = 'https://storage.com';
    it('logoUrl', function () {
      var key = 'fooEq112ewq134qweq41weqweqwe';
      setLogoKey(key);
      expect(parsedRecord.logoUrl(storageUrl)).to.equal("".concat(storageUrl, "/").concat(key));
    });
    it('termsUrl', function () {
      var key = 'barEq112ewq134qweq41weqweqwe';
      setTermsKey(key);
      expect(parsedRecord.termsUrl(storageUrl)).to.equal("".concat(storageUrl, "/").concat(key));
    });
  });
  describe('opts() method should return proper data', function () {
    var opts;
    beforeEach(function () {
      opts = parsedRecord.opts();
    });
    it('requestID', function () {
      expect(opts.requestID).to.equal(rawRecord.id);
    });
    it('code', function () {
      expect(opts.code).to.equal(rawRecord.details.asset_create.code);
    });
    it('initialPreissuedAmount', function () {
      expect(opts.initialPreissuedAmount).to.equal(rawRecord.details.asset_create.initial_preissued_amount);
    });
    it('preissuedAssetSigner', function () {
      expect(opts.preissuedAssetSigner).to.equal(rawRecord.details.asset_create.pre_issued_asset_signer);
    });
    it('maxIssuanceAmount', function () {
      expect(opts.maxIssuanceAmount).to.equal(rawRecord.details.asset_create.max_issuance_amount);
    });
    it('name', function () {
      expect(opts.details.name).to.equal(rawRecord.details.asset_create.details.name);
    });
    it('policies', function () {
      var expectedPolicy = 7;
      setPolicies(1, 2, 4);
      opts = parsedRecord.opts();
      expect(opts.policies).to.equal(expectedPolicy);
    });
    it('logo', function () {
      expect(opts.details.logo.key).to.equal(rawRecord.details.asset_create.details.logo.key);
      expect(opts.details.logo.type).to.equal(rawRecord.details.asset_create.details.logo.type);
      expect(opts.details.logo.name).to.equal(rawRecord.details.asset_create.details.logo.name);
    });
    it('terms', function () {
      expect(opts.details.terms.key).to.equal(rawRecord.details.asset_create.details.terms.key);
      expect(opts.details.terms.type).to.equal(rawRecord.details.asset_create.details.terms.type);
      expect(opts.details.terms.name).to.equal(rawRecord.details.asset_create.details.terms.name);
    });
  });
});