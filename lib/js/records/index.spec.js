"use strict";

var _test = require("./test");

var _index = require("./index");

describe('Record factory', function () {
  var testRequest = function testRequest(request, constructorName) {
    expect(_index.RecordFactory.request(_test.MockFactory.horizon(request))).to.be.instanceOf(_index.RecordFactory.constructors[constructorName]);
  };

  it('should properly define asset_create request', function () {
    testRequest(_test.MockFactory.assetCreate, 'AssetCreateRequestRecord');
  });
  it('should properly define asset_update request', function () {
    testRequest(_test.MockFactory.assetUpdate, 'AssetUpdateRequestRecord');
  });
  it('should properly define sale request', function () {
    testRequest(_test.MockFactory.saleCreate, 'SaleRequestRecord');
  });
  it('should properly define update_sale_details request', function () {
    testRequest(_test.MockFactory.updateSaleDetails, 'UpdateSaleDetailsRequestRecord');
  });
});