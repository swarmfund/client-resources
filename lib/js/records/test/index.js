"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MockFactory = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _assetCreate = _interopRequireDefault(require("./mocks/asset-create"));

var _assetUpdate = _interopRequireDefault(require("./mocks/asset-update"));

var _saleCreate = _interopRequireDefault(require("./mocks/sale-create"));

var _updateSaleDetails = _interopRequireDefault(require("./mocks/update-sale-details"));

var _swarmNewSdk = require("swarm-new-sdk");

var MockFactory =
/*#__PURE__*/
function () {
  function MockFactory() {
    (0, _classCallCheck2.default)(this, MockFactory);
  }

  (0, _createClass2.default)(MockFactory, null, [{
    key: "horizon",
    value: function horizon(record) {
      return new _swarmNewSdk.HorizonResponse({
        data: record
      }, {}).data;
    }
  }, {
    key: "api",
    value: function api(record) {
      return new _swarmNewSdk.ApiResponse({
        data: record
      }, {}).data;
    }
  }, {
    key: "assetCreate",
    get: function get() {
      return _assetCreate.default;
    }
  }, {
    key: "assetUpdate",
    get: function get() {
      return _assetUpdate.default;
    }
  }, {
    key: "saleCreate",
    get: function get() {
      return _saleCreate.default;
    }
  }, {
    key: "updateSaleDetails",
    get: function get() {
      return _updateSaleDetails.default;
    }
  }]);
  return MockFactory;
}();

exports.MockFactory = MockFactory;