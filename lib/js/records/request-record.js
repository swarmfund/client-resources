"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RequestRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _requestStates = require("../const/request-states.const");

var _get2 = _interopRequireDefault(require("lodash/get"));

var RequestRecord =
/*#__PURE__*/
function () {
  function RequestRecord() {
    var record = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    (0, _classCallCheck2.default)(this, RequestRecord);
    this._record = record;
    this.id = record.id || '0';
    this.requestor = record.requestor;
    this.reviewer = record.reviewer;
    this.reference = record.reference;
    this.rejectReason = record.rejectReason;
    this.hash = record.hash;
    this.createdAt = record.createdAt;
    this.updatedAt = record.updatedAt;
    this.state = record.requestState;
    this.stateI = record.requestStateI;
    this.requestType = (0, _get2.default)(record, 'details.requestType');
    this.requestTypeI = (0, _get2.default)(record, 'details.requestTypeI');
    this.twoStepWithdrawal = (0, _get2.default)(record, 'details.twoStepWithdrawal');
    this.limitsUpdate = (0, _get2.default)(record, 'details.limitsUpdate');
    this.amlAlert = (0, _get2.default)(record, 'details.amlAlert');
    this.updateSaleDetails = (0, _get2.default)(record, 'details.updateSaleDetails');
    this.updateSaleEndTime = (0, _get2.default)(record, 'details.updateSaleEndTime');
    this.promotionUpdateRequest = (0, _get2.default)(record, 'details.promotionUpdateRequest');
  }

  (0, _createClass2.default)(RequestRecord, [{
    key: "isPending",
    get: function get() {
      return this.stateI === _requestStates.REQUEST_STATES.pending;
    }
  }, {
    key: "isApproved",
    get: function get() {
      return this.stateI === _requestStates.REQUEST_STATES.approved;
    }
  }, {
    key: "isCancelled",
    get: function get() {
      return this.stateI === _requestStates.REQUEST_STATES.cancelled;
    }
  }, {
    key: "isRejected",
    get: function get() {
      return this.stateI === _requestStates.REQUEST_STATES.rejected;
    }
  }, {
    key: "isPermanentlyRejected",
    get: function get() {
      return this.stateI === _requestStates.REQUEST_STATES.permanentlyRejected;
    }
  }]);
  return RequestRecord;
}();

exports.RequestRecord = RequestRecord;