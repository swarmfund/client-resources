"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WithdrawRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _opRecord = require("../op-record");

var _get2 = _interopRequireDefault(require("lodash/get"));

var WithdrawRecord =
/*#__PURE__*/
function (_OpRecord) {
  (0, _inherits2.default)(WithdrawRecord, _OpRecord);

  function WithdrawRecord(record) {
    var _this;

    (0, _classCallCheck2.default)(this, WithdrawRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(WithdrawRecord).call(this, record));
    _this.name = 'Withdrawal';
    _this.amount = record.amount;
    _this.asset = record.destAsset;
    _this.fixedFee = record.feeFixed;
    _this.percentFee = record.feePercent;
    _this.feeAsset = record.destAsset;
    _this.counterparty = (0, _get2.default)(record, 'externalDetails.address');
    _this.direction = 'out';
    _this.destinationAsset = record.destAsset;
    _this.destinationAmount = record.destAmount;
    _this.identifier = record.identifier;
    return _this;
  }

  return WithdrawRecord;
}(_opRecord.OpRecord);

exports.WithdrawRecord = WithdrawRecord;