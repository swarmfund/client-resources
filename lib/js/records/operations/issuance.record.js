"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IssuanceRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _opRecord = require("../op-record");

var _get2 = _interopRequireDefault(require("lodash/get"));

var NAMES_BY_CAUSE = {
  'airdrop': 'Registration Promo Airdrop',
  'airdrop-for-kyc': 'KYC verification bonus',
  'airdrop-telegram': 'Telegram airdrop',
  'none': 'Deposit'
};

var IssuanceRecord =
/*#__PURE__*/
function (_OpRecord) {
  (0, _inherits2.default)(IssuanceRecord, _OpRecord);

  function IssuanceRecord(record) {
    var _this;

    (0, _classCallCheck2.default)(this, IssuanceRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(IssuanceRecord).call(this, record));
    _this.amount = record.amount;
    _this.asset = record.asset;
    _this.direction = 'in';
    _this.feeAsset = record.asset;
    _this.fixedFee = record.feeFixed;
    _this.percentFee = record.feePercent;
    _this.id = record.id;
    _this.subject = record.reference;
    _this.externalDetails = record.externalDetails;
    _this.blockNumber = (0, _get2.default)(record, 'externalDetails.blockNumber');
    _this.outIndex = (0, _get2.default)(record, 'externalDetails.outIndex');
    _this.txHash = (0, _get2.default)(record, 'externalDetails.txHash');
    return _this;
  }

  (0, _createClass2.default)(IssuanceRecord, [{
    key: "name",
    get: function get() {
      return NAMES_BY_CAUSE[(0, _get2.default)(this._record, 'externalDetails.cause')] || NAMES_BY_CAUSE.none;
    }
  }, {
    key: "counterparty",
    get: function get() {
      return (0, _get2.default)(this._record.participants.find(function (p) {
        return !p.balance_id;
      }), 'accountId');
    }
  }, {
    key: "isDeposit",
    get: function get() {
      return this.name === NAMES_BY_CAUSE.none;
    }
  }]);
  return IssuanceRecord;
}(_opRecord.OpRecord);

exports.IssuanceRecord = IssuanceRecord;