"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.PaymentRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _opRecord = require("../op-record");

var _math = require("../../utils/math.util");

var _get2 = _interopRequireDefault(require("lodash/get"));

var PaymentRecord =
/*#__PURE__*/
function (_OpRecord) {
  (0, _inherits2.default)(PaymentRecord, _OpRecord);

  /**
   * @param record - raw record from {@link HorizonResponse}
   * @param details
   * @param details.accountId
   */
  function PaymentRecord(record) {
    var _this;

    var details = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    (0, _classCallCheck2.default)(this, PaymentRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(PaymentRecord).call(this, record));
    _this.name = 'Transfer';
    _this.amount = record.amount;
    _this.asset = record.asset;
    _this.direction = _this._checkDirection(details.accountId);
    _this.counterparty = _this._parseCounterParty();
    _this.fees = _this._calculateFees();
    _this.sourcePaysForDest = record.sourcePaysForDest;
    _this.sourceFeeAsset = (0, _get2.default)(record, 'sourceFeeData.actualPaymentFeeAssetCode');
    _this.destinationFeeAsset = (0, _get2.default)(record, 'destinationFeeData.actualPaymentFeeAssetCode');
    _this.participants = _this._parseParticipants();
    _this.receiver = record.to;
    _this.sender = record.from;
    _this.subject = record.subject;
    return _this;
  }

  (0, _createClass2.default)(PaymentRecord, [{
    key: "_checkDirection",
    value: function _checkDirection(accountId) {
      return this.sender === accountId ? 'out' : 'in';
    }
  }, {
    key: "_calculateFees",
    value: function _calculateFees() {
      return {
        source: _math.MathUtil.add((0, _get2.default)(this._record, 'sourceFeeData.actualPaymentFee'), (0, _get2.default)(this._record, 'sourceFeeData.fixedFee')),
        destination: _math.MathUtil.add((0, _get2.default)(this._record, 'destinationFeeData.actualPaymentFee'), (0, _get2.default)(this._record, 'destinationFeeData.fixedFee'))
      };
    }
  }, {
    key: "_parseParticipants",
    value: function _parseParticipants() {
      return this._record.participants.map(function (participant) {
        return participant.account_id;
      });
    }
  }, {
    key: "_parseCounterParty",
    value: function _parseCounterParty() {
      var direction = this._checkDirection();

      return direction === 'in' ? this._record.from : this._record.to;
    }
  }]);
  return PaymentRecord;
}(_opRecord.OpRecord);

exports.PaymentRecord = PaymentRecord;