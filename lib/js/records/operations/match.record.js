"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MatchEffect = exports.MatchRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _opRecord = require("../op-record");

var _get2 = _interopRequireDefault(require("lodash/get"));

var DIRECTIONS_VERBOSE = {
  in: 'in',
  out: 'out'
};

var MatchRecord =
/*#__PURE__*/
function (_OpRecord) {
  (0, _inherits2.default)(MatchRecord, _OpRecord);

  /**
   * @param record
   * @param {object} details
   * @param {string} details.asset
   * @param {bool} details.isSaleMatch
   * @param {string} details.balanceId
   * @param {string} details.accountId
   */
  function MatchRecord(record, details) {
    var _this;

    (0, _classCallCheck2.default)(this, MatchRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(MatchRecord).call(this, record));
    _this.asset = details.asset;
    _this.isSaleMatch = details.isSaleMatch;
    _this.balanceId = details.balanceId;
    _this.accountId = details.accountId;
    _this.participants = record.participants;
    _this.participant = _this._getParticipant();
    _this.effects = _this._getEffects();
    return _this;
  }

  (0, _createClass2.default)(MatchRecord, [{
    key: "_getParticipant",
    value: function _getParticipant() {
      var _this2 = this;

      return this.participants.find(function (participant) {
        return participant.accountId === _this2.accountId && participant.balanceId === _this2.balanceId;
      }) || {};
    }
  }, {
    key: "_getEffects",
    value: function _getEffects() {
      var _this3 = this;

      return (0, _get2.default)(this.participant, 'effects', []).map(function (effect) {
        return new MatchEffect(effect, {
          isSaleMatch: _this3.isSaleMatch,
          asset: _this3.asset,
          date: _this3.date,
          name: _this3.name,
          feeAsset: _this3.asset
        });
      });
    }
  }]);
  return MatchRecord;
}(_opRecord.OpRecord);

exports.MatchRecord = MatchRecord;

var MatchEffect =
/*#__PURE__*/
function () {
  function MatchEffect(effect, opts) {
    (0, _classCallCheck2.default)(this, MatchEffect);
    this._effect = effect;
    this.asset = opts.asset;
    this.name = opts.name;
    this.date = opts.date;
    this.feeAsset = opts.feeAsset;
    this.isSaleMatch = opts.isSaleMatch;
    this.baseAsset = effect.baseAsset;
    this.quoteAsset = effect.quoteAsset;
    this.counterparty = "".concat(effect.baseAsset, " fund");
    this.isBuy = effect.isBuy;
    this.matches = effect.matches;
    this.quoteAmount = this._getQuoteAmount();
    this.baseAmount = this._getBaseAmount();
    this.price = this._getPrice();
    this.feePaid = this._getFeePaid();
    this.amount = this._getAmount();
    this.direction = this._getDirection();
    this.name = this._getName();
  }

  (0, _createClass2.default)(MatchEffect, [{
    key: "_getQuoteAmount",
    value: function _getQuoteAmount() {
      return this._effect.matches.reduce(function (sum, match) {
        return match.quoteAmount + sum;
      }, 0);
    }
  }, {
    key: "_getBaseAmount",
    value: function _getBaseAmount() {
      return this._effect.matches.reduce(function (sum, match) {
        return match.baseAmount + sum;
      }, 0);
    }
  }, {
    key: "_getAmount",
    value: function _getAmount() {
      if (this.asset === this.quoteAsset) {
        return this.quoteAmount;
      }

      return this.baseAmount;
    }
  }, {
    key: "_getPrice",
    value: function _getPrice() {
      return this.matches[0].price;
    }
  }, {
    key: "_getFeePaid",
    value: function _getFeePaid() {
      return this.matches.reduce(function (sum, match) {
        return match.feePaid + sum;
      }, 0);
    }
  }, {
    key: "_getDirection",
    value: function _getDirection() {
      if (this.asset === this.quoteAsset) {
        return this.isBuy ? DIRECTIONS_VERBOSE.out : DIRECTIONS_VERBOSE.in;
      }

      return this.isBuy ? DIRECTIONS_VERBOSE.in : DIRECTIONS_VERBOSE.out;
    }
  }, {
    key: "_getName",
    value: function _getName() {
      if (!this.isSaleMatch) {
        return 'Order match';
      } else if (this.direction === DIRECTIONS_VERBOSE.in) {
        return 'Tokens received';
      } else {
        return 'Investment';
      }
    }
  }]);
  return MatchEffect;
}();

exports.MatchEffect = MatchEffect;