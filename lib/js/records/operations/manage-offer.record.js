"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ManageOfferRecord = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/possibleConstructorReturn"));

var _getPrototypeOf2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/getPrototypeOf"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/inherits"));

var _opRecord = require("../op-record");

var _utils = require("../../utils");

var STATES = {
  canceled: 'canceled',
  pending: 'pending'
};

var ManageOfferRecord =
/*#__PURE__*/
function (_OpRecord) {
  (0, _inherits2.default)(ManageOfferRecord, _OpRecord);

  function ManageOfferRecord(record, details) {
    var _this;

    (0, _classCallCheck2.default)(this, ManageOfferRecord);
    _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(ManageOfferRecord).call(this, record));
    _this.asset = details.asset;
    _this.balanceId = details.balanceId;
    _this.accountId = details.accountId;
    _this.participants = record.participants;
    _this.isBuy = record.isBuy;
    _this.amount = _utils.MathUtil.multiply(record.amount, record.price);
    _this.price = record.price;
    _this.fee = record.fee;
    _this.offerI = record.offerI;
    _this.orderBookId = record.orderBookId;
    _this.isDeleted = record.isDeleted;
    _this.baseAsset = record.baseAsset;
    _this.counterparty = _this.baseAsset;
    return _this;
  }

  (0, _createClass2.default)(ManageOfferRecord, [{
    key: "direction",
    get: function get() {
      return this.state === STATES.canceled ? 'in' : 'out';
    }
  }, {
    key: "name",
    get: function get() {
      return this.state === STATES.canceled ? 'Canceled allocation' : 'Allocation';
    }
  }]);
  return ManageOfferRecord;
}(_opRecord.OpRecord);

exports.ManageOfferRecord = ManageOfferRecord;