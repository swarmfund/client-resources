"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RecordFactory = void 0;

var _construct2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/construct"));

var _objectSpread2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/objectSpread"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _swarmNewSdk = require("swarm-new-sdk");

var _requestRecord = require("./request-record");

var _assetCreate = require("./requests/asset-create.record");

var _assetUpdate = require("./requests/asset-update.record");

var _saleCreate = require("./requests/sale-create.record");

var _updateSaleDetails = require("./requests/update-sale-details.record");

var _opRecord = require("./op-record");

var _issuance = require("./operations/issuance.record");

var _withdraw = require("./operations/withdraw.record");

var _payment = require("./operations/payment.record");

var _match = require("./operations/match.record");

var _manageOffer = require("./operations/manage-offer.record");

var _sale = require("./entities/sale.record");

var RecordFactory =
/*#__PURE__*/
function () {
  function RecordFactory() {
    (0, _classCallCheck2.default)(this, RecordFactory);
  }

  (0, _createClass2.default)(RecordFactory, null, [{
    key: "operation",
    value: function operation(record, details) {
      switch (record.typeI) {
        case _swarmNewSdk.OP_TYPES.payment:
          return this.createPaymentRecord.apply(this, arguments);

        case _swarmNewSdk.OP_TYPES.createIssuanceRequest:
          return this.createIssuanceRecord.apply(this, arguments);

        case _swarmNewSdk.OP_TYPES.createWithdrawalRequest:
          return this.createWithdrawRecord.apply(this, arguments);

        case _swarmNewSdk.OP_TYPES.manageOffer:
          return this.createManageOfferRecord.apply(this, arguments);

        case _swarmNewSdk.OP_TYPES.checkSaleState:
          return this.createMatchRecord(record, (0, _objectSpread2.default)({}, details, {
            isSaleMatch: true
          }));

        case _swarmNewSdk.OP_TYPES.paymentV2:
          return this.createPaymentRecord.apply(this, arguments);

        default:
          return (0, _construct2.default)(_opRecord.OpRecord, Array.prototype.slice.call(arguments));
      }
    }
  }, {
    key: "request",
    value: function request(record, details) {
      switch (record.details.requestTypeI) {
        case _swarmNewSdk.REQUEST_TYPES.assetCreate:
          return this.createAssetCreateRequestRecord.apply(this, arguments);

        case _swarmNewSdk.REQUEST_TYPES.assetUpdate:
          return this.createAssetUpdateRequestRecord.apply(this, arguments);

        case _swarmNewSdk.REQUEST_TYPES.preIssuanceCreate:
        case _swarmNewSdk.REQUEST_TYPES.issuanceCreate:
        case _swarmNewSdk.REQUEST_TYPES.withdraw:
        case _swarmNewSdk.REQUEST_TYPES.sale:
          return this.createSaleRequestRecord.apply(this, arguments);

        case _swarmNewSdk.REQUEST_TYPES.limitsUpdate:
        case _swarmNewSdk.REQUEST_TYPES.twoStepWithdrawal:
        case _swarmNewSdk.REQUEST_TYPES.amlAlert:
        case _swarmNewSdk.REQUEST_TYPES.updateKyc:
        case _swarmNewSdk.REQUEST_TYPES.updateSaleDetail:
          return this.createUpdateSaleDetailsRequestRecord.apply(this, arguments);

        default:
          return (0, _construct2.default)(_requestRecord.RequestRecord, Array.prototype.slice.call(arguments));
      }
    }
  }, {
    key: "createPaymentRecord",

    /** operations: **/
    value: function createPaymentRecord() {
      return (0, _construct2.default)(_payment.PaymentRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createIssuanceRecord",
    value: function createIssuanceRecord() {
      return (0, _construct2.default)(_issuance.IssuanceRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createWithdrawRecord",
    value: function createWithdrawRecord() {
      return (0, _construct2.default)(_withdraw.WithdrawRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createManageOfferRecord",
    value: function createManageOfferRecord(record) {
      if (record.orderBookId && record.orderBookId !== '0') {
        return (0, _construct2.default)(_manageOffer.ManageOfferRecord, Array.prototype.slice.call(arguments));
      }

      return (0, _construct2.default)(_match.MatchRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createMatchRecord",
    value: function createMatchRecord() {
      return (0, _construct2.default)(_match.MatchRecord, Array.prototype.slice.call(arguments));
    }
    /** requests: **/

  }, {
    key: "createAssetCreateRequestRecord",
    value: function createAssetCreateRequestRecord() {
      return (0, _construct2.default)(_assetCreate.AssetCreateRequestRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createSaleRequestRecord",
    value: function createSaleRequestRecord() {
      return (0, _construct2.default)(_saleCreate.SaleRequestRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createAssetUpdateRequestRecord",
    value: function createAssetUpdateRequestRecord() {
      return (0, _construct2.default)(_assetUpdate.AssetUpdateRequestRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "createUpdateSaleDetailsRequestRecord",
    value: function createUpdateSaleDetailsRequestRecord() {
      return (0, _construct2.default)(_updateSaleDetails.UpdateSaleDetailsRequestRecord, Array.prototype.slice.call(arguments));
    }
    /** entities **/

  }, {
    key: "createSaleRecord",
    value: function createSaleRecord() {
      return (0, _construct2.default)(_sale.SaleRecord, Array.prototype.slice.call(arguments));
    }
  }, {
    key: "constructors",
    get: function get() {
      return {
        IssuanceRecord: _issuance.IssuanceRecord,
        WithdrawRecord: _withdraw.WithdrawRecord,
        PaymentRecord: _payment.PaymentRecord,
        MatchRecord: _match.MatchRecord,
        ManageOfferRecord: _manageOffer.ManageOfferRecord,
        AssetCreateRequestRecord: _assetCreate.AssetCreateRequestRecord,
        SaleRequestRecord: _saleCreate.SaleRequestRecord,
        AssetUpdateRequestRecord: _assetUpdate.AssetUpdateRequestRecord,
        UpdateSaleDetailsRequestRecord: _updateSaleDetails.UpdateSaleDetailsRequestRecord,
        SaleRecord: _sale.SaleRecord
      };
    }
  }]);
  return RecordFactory;
}();

exports.RecordFactory = RecordFactory;