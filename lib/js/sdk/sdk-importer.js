"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Sdk = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime-corejs2/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs2/helpers/createClass"));

var _swarmNewSdk = require("swarm-new-sdk");

var _sdkInstance = null;

var Sdk =
/*#__PURE__*/
function () {
  function Sdk() {
    (0, _classCallCheck2.default)(this, Sdk);
  }

  (0, _createClass2.default)(Sdk, null, [{
    key: "init",
    value: function () {
      var _init = (0, _asyncToGenerator2.default)(
      /*#__PURE__*/
      _regenerator.default.mark(function _callee(serverUrl) {
        return _regenerator.default.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.next = 2;
                return _swarmNewSdk.Swarm.create(serverUrl);

              case 2:
                _sdkInstance = _context.sent;
                return _context.abrupt("return", _sdkInstance);

              case 4:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      return function init(_x) {
        return _init.apply(this, arguments);
      };
    }()
  }, {
    key: "sdk",
    get: function get() {
      return _sdkInstance;
    }
  }, {
    key: "horizon",
    get: function get() {
      return _sdkInstance.horizon;
    }
  }, {
    key: "api",
    get: function get() {
      return _sdkInstance.api;
    }
  }]);
  return Sdk;
}();

exports.Sdk = Sdk;