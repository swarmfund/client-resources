"use strict";

var _Object$defineProperty = require("@babel/runtime-corejs2/core-js/object/define-property");

var _Object$keys = require("@babel/runtime-corejs2/core-js/object/keys");

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _js = require("./js");

_Object$keys(_js).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;

  _Object$defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _js[key];
    }
  });
});