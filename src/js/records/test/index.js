import assetCreate from './mocks/asset-create'
import assetUpdate from './mocks/asset-update'
import saleCreate from './mocks/sale-create'
import updateSaleDetails from './mocks/update-sale-details'

import { HorizonResponse, ApiResponse } from 'swarm-new-sdk'

export class MockFactory {
  static horizon (record) { return (new HorizonResponse({ data: record }, {})).data }
  static api (record) { return (new ApiResponse({ data: record }, {})).data }

  static get assetCreate () { return assetCreate }
  static get assetUpdate () { return assetUpdate }
  static get saleCreate () { return saleCreate }
  static get updateSaleDetails () { return updateSaleDetails }
}
