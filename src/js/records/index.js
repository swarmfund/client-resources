import {
  OP_TYPES,
  REQUEST_TYPES
} from 'swarm-new-sdk'

import { RequestRecord } from './request-record'
import { AssetCreateRequestRecord } from './requests/asset-create.record'
import { AssetUpdateRequestRecord } from './requests/asset-update.record'
import { SaleRequestRecord } from './requests/sale-create.record'
import { UpdateSaleDetailsRequestRecord } from './requests/update-sale-details.record'

import { OpRecord } from './op-record'
import { IssuanceRecord } from './operations/issuance.record'
import { WithdrawRecord } from './operations/withdraw.record'
import { PaymentRecord } from './operations/payment.record'
import { MatchRecord } from './operations/match.record'
import { ManageOfferRecord } from './operations/manage-offer.record'

import { SaleRecord } from './entities/sale.record'

export class RecordFactory {
  static operation (record, details) {
    switch (record.typeI) {
      case OP_TYPES.payment:
        return this.createPaymentRecord(...arguments)
      case OP_TYPES.createIssuanceRequest:
        return this.createIssuanceRecord(...arguments)
      case OP_TYPES.createWithdrawalRequest:
        return this.createWithdrawRecord(...arguments)
      case OP_TYPES.manageOffer:
        return this.createManageOfferRecord(...arguments)
      case OP_TYPES.checkSaleState:
        return this.createMatchRecord(record, { ...details, isSaleMatch: true })
      case OP_TYPES.paymentV2:
        return this.createPaymentRecord(...arguments)
      default:
        return new OpRecord(...arguments)
    }
  }

  static request (record, details) {
    switch (record.details.requestTypeI) {
      case REQUEST_TYPES.assetCreate:
        return this.createAssetCreateRequestRecord(...arguments)
      case REQUEST_TYPES.assetUpdate:
        return this.createAssetUpdateRequestRecord(...arguments)
      case REQUEST_TYPES.preIssuanceCreate:
      case REQUEST_TYPES.issuanceCreate:
      case REQUEST_TYPES.withdraw:
      case REQUEST_TYPES.sale:
        return this.createSaleRequestRecord(...arguments)
      case REQUEST_TYPES.limitsUpdate:
      case REQUEST_TYPES.twoStepWithdrawal:
      case REQUEST_TYPES.amlAlert:
      case REQUEST_TYPES.updateKyc:
      case REQUEST_TYPES.updateSaleDetail:
        return this.createUpdateSaleDetailsRequestRecord(...arguments)
      default:
        return new RequestRecord(...arguments)
    }
  }

  static get constructors () {
    return {
      IssuanceRecord,
      WithdrawRecord,
      PaymentRecord,
      MatchRecord,
      ManageOfferRecord,
      AssetCreateRequestRecord,
      SaleRequestRecord,
      AssetUpdateRequestRecord,
      UpdateSaleDetailsRequestRecord,
      SaleRecord
    }
  }

  /** operations: **/

  static createPaymentRecord () {
    return new PaymentRecord(...arguments)
  }

  static createIssuanceRecord () {
    return new IssuanceRecord(...arguments)
  }

  static createWithdrawRecord () {
    return new WithdrawRecord(...arguments)
  }

  static createManageOfferRecord (record) {
    if (record.orderBookId && record.orderBookId !== '0') {
      return new ManageOfferRecord(...arguments)
    }

    return new MatchRecord(...arguments)
  }

  static createMatchRecord () {
    return new MatchRecord(...arguments)
  }

  /** requests: **/
  static createAssetCreateRequestRecord () {
    return new AssetCreateRequestRecord(...arguments)
  }

  static createSaleRequestRecord () {
    return new SaleRequestRecord(...arguments)
  }

  static createAssetUpdateRequestRecord () {
    return new AssetUpdateRequestRecord(...arguments)
  }

  static createUpdateSaleDetailsRequestRecord () {
    return new UpdateSaleDetailsRequestRecord(...arguments)
  }

  /** entities **/

  static createSaleRecord () {
    return new SaleRecord(...arguments)
  }
}
