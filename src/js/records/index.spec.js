import { MockFactory } from './test'
import { RecordFactory } from './index'

describe('Record factory', () => {
  const testRequest = (request, constructorName) => {
    expect(RecordFactory.request(MockFactory.horizon(request)))
      .to.be
      .instanceOf(RecordFactory.constructors[constructorName])
  }

  it('should properly define asset_create request', () => {
    testRequest(MockFactory.assetCreate, 'AssetCreateRequestRecord')
  })

  it('should properly define asset_update request', () => {
    testRequest(MockFactory.assetUpdate, 'AssetUpdateRequestRecord')
  })

  it('should properly define sale request', () => {
    testRequest(MockFactory.saleCreate, 'SaleRequestRecord')
  })

  it('should properly define update_sale_details request', () => {
    testRequest(MockFactory.updateSaleDetails, 'UpdateSaleDetailsRequestRecord')
  })
})
