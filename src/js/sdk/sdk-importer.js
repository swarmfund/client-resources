import { Swarm } from 'swarm-new-sdk'

let _sdkInstance = null

export class Sdk {
  static async init (serverUrl) {
    _sdkInstance = await Swarm.create(serverUrl)
    return _sdkInstance
  }

  static get sdk () {
    return _sdkInstance
  }

  static get horizon () {
    return _sdkInstance.horizon
  }

  static get api () {
    return _sdkInstance.api
  }
}
